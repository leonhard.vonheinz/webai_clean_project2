# WEB-AI Movie recommender system

This is Group 19's Project 2 **Movie recommender system**.

## Authors and acknowledgment
Alexander Gerhard Ditz

Nils Niehaus

Leonard von Heiz

## Description

This 'python' project consists of:

- A **Flask app** 
- A **Surprise library recommender**

## Requirements

You need a 'python' environment with the packages:
- flask
- flask-sqlalchemy
- flask-user==1.0.2.2
- scikit-learn
- scikit-surprise
installed,
as well as an internet connection.

-> As of right now, we could not compile all packages for a working venv! 
The code was written and run in conda and we thank you in advance if you can check the project with another kernel. 

## Usage

To use the recommender app from your local directory, **run 'flask --app recommender run'** in the terminal.


## Features

The app provides a ui with the basic routes and Object-Relation models adjusted to support the external recommender working with the flask-sqlalchemy session.

With an initialized sqlite DB using models.py, the flask-sqlalchemy session registers and persistently records new user-ratings. 
The Rating model receives 'One-to-Many' db.relations from Movie and User models, on the Rating.id column.

User recommendations are generated and displayed in "/recommendations".

## Implementation

The flask app imports a 'Surprise' implementation of an item-based cosine similarity recommendation mechanism, from 'sim_class.py' 
(earlier versions: 'surprise_reco.py', 'reco_class.py', 'reco_slim.py').

Some versions use class attributes to instantiate and evaluate a 'Surprise' 'training_set' and a 'similarity_matrix', on being imported by 'recommender.py', based on "data/ratings.csv", unfortunately still encountering problems with repeated recomputations being triggered, and having separate channels when handling flask-app ratings for current users and and csv ratings for computing item-similarities.

A user's ratings are queried when clicking on "Recommend" on "/movies". 
Selecting the 'k=20' highest rated movies decides k item-similarities-vectors, 
that are weighted by the user-rating score given.
Summed over the k items, this produces a vector of estimated relevance/item-similarity for the user, from which 'num_recs=10' highest scoring items are selected.

Recommended items are serialized in a 'data/user_timestamp.csv', and returned to flask.

Recommendations are displayed in "/recommendations"



## Gitlab

https://gitlab.gwdg.de/leonhard.vonheinz/webai_clean_project2


## TODOS

- display avg ratings
- save ratings to db
- searchable tags, searchable titles?
- no duplicate tags
- rank tags in search function
- display user history/ratings (used as recommendation basis)


