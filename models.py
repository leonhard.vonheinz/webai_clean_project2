from flask_sqlalchemy import SQLAlchemy
from flask_user import UserMixin

db = SQLAlchemy()

# Define the User data-model.
# NB: Make sure to add flask_user UserMixin as this adds additional fields and properties required by Flask-User
class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')

    # User authentication information. The collation='NOCASE' is required
    # to search case insensitively when USER_IFIND_MODE is 'nocase_collation'.
    username = db.Column(db.String(100, collation='NOCASE'), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')
    email_confirmed_at = db.Column(db.DateTime())

    # User information
    first_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    last_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
    
    rating_ids = db.Column(db.Integer, nullable=True) 
    rating = db.relationship('Rating', backref='author', lazy=True, primaryjoin='User.rating_ids == Rating.id',
                             foreign_keys=rating_ids)
    
    rated_movie_ids = db.Column(db.Integer, nullable=True)
    rated_movie = db.relationship('Movie', backref='rating_author', lazy=True, primaryjoin='User.rated_movie_ids == Movie.rating_ids',
                                  foreign_keys=rated_movie_ids) 

## Movies database-models
class Movie(db.Model):
    __tablename__ = 'movies'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100, collation='NOCASE'), nullable=False, unique=True)
    genres = db.relationship('MovieGenre', backref='movie', lazy=True) 
    links = db.relationship('Links', backref='movie', lazy=True)
    tags = db.relationship('Tag', backref='movie', lazy=True) 
    
    rating_ids = db.Column(db.Integer, nullable=True) 
    rating = db.relationship('Rating', backref='subject', lazy=True, primaryjoin='Movie.rating_ids == Rating.id',
                             foreign_keys=rating_ids)
    

class MovieGenre(db.Model):
    __tablename__ = 'movie_genres'
    id = db.Column(db.Integer, primary_key=True)
    movie_id = db.Column(db.Integer, db.ForeignKey('movies.id'), nullable=False)
    genre = db.Column(db.String(255), nullable=False, server_default='')


## Links database-models
class Links(db.Model):
    __tablename__ = 'links'
    id = db.Column(db.Integer, primary_key=True)
    movie_id = db.Column(db.Integer, db.ForeignKey('movies.id'), nullable=False)
    imdb_id = db.Column(db.Integer, server_default='')
    tmdb_id = db.Column(db.Integer, server_default='')


## Tags database-models
class Tag(db.Model):
    __tablename__ = 'tags'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    movie_id = db.Column(db.Integer, db.ForeignKey('movies.id'), nullable=False)
    tag = db.Column(db.String(255, collation='NOCASE'), server_default='')
    timestamp = db.Column(db.Integer)

## Ratings database-models
class Rating(db.Model):
    __tablename__ = 'ratings'
    id = db.Column(db.Integer, primary_key=True)    
    rating = db.Column(db.String(255, collation='NOCASE'), server_default='')
    timestamp = db.Column(db.Integer)
    

    user_id = db.Column(db.Integer, nullable=False)
    movie_id = db.Column(db.Integer, nullable=False)
    
    # __repr__():
    #    return self._repr(id=self.id, user_id=self.user_id, movie_id=self.movie_id)
        
    
