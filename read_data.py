import csv
import datetime
from sqlalchemy.exc import IntegrityError
from models import db, Movie, MovieGenre, Links, Tag, Rating, User

def check_and_read_data(db):
    # check if we have movies in the database
    # read data if database is empty
    #TODO Is Movie instantiated here? or is db? TODO rename db??
    if Movie.query.count() == 0: 
        # read movies from csv
        with open('data/movies.csv', newline='', encoding='utf8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
           # count = 0
            for row in reader:
                #if count > 0:
                    try:
                        id = row[0]
                        title = row[1]
                        movie = Movie(id=id, title=title)
                        db.session.add(movie)
                        genres = row[2].split('|')  # genres is a list of genres
                        for genre in genres:  # add each genre to the movie_genre table
                            movie_genre = MovieGenre(movie_id=id, genre=genre)
                            db.session.add(movie_genre)
                        db.session.commit()  # save data to database
                    except IntegrityError:
                        print("Ignoring duplicate movie: " + title)
                        db.session.rollback()
                        pass    
                #count += 1
                #if count % 100 == 0:
                #    print(count, " movies read")

    if Links.query.count() == 0:

        with open('data/links.csv', newline='', encoding='utf8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                movie_id = row[0]
                imdb_id = row[1]
                tmdb_id = row[2]
                links = Links(movie_id=movie_id, imdb_id=imdb_id, tmdb_id=tmdb_id)
                db.session.add(links)
                db.session.commit()  # save data to database


    if Tag.query.count() == 0:

        with open('data/tags.csv', newline='', encoding='utf8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                user_id = row[0]
                movie_id = row[1]
                tag = row[2]
                timestamp= row[3]
                tag = Tag(user_id = user_id, movie_id=movie_id, tag=tag, timestamp=timestamp)
                db.session.add(tag)
                db.session.commit()  # save data to database

            
    if Rating.query.count() == 0:

        with open('data/ratings_compact.csv', newline='', encoding='utf8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            user_id = 0
            for row in reader:
                if user_id != row[0]:
                    user = User(username = row[0], email_confirmed_at = None)
                    db.session.add(user)
                user_id = row[0]
                movie_id = row[1]
                rating = row[2]
                timestamp= row[3]
                rating = Rating(user_id = user_id, movie_id=movie_id, rating=rating, timestamp=timestamp)
                db.session.add(rating)
                db.session.commit()  # save data to database

            


