from surprise import KNNBasic, Dataset, Reader
import csv
import os
import heapq
from collections import defaultdict
from operator import itemgetter

import pandas as pd
from datetime import datetime

import numpy as np
#from copy import deepcopy


###################
# HELPER FUNCTIONS 
###################

def load_ds():
    """
    Read ratings.csv into a surprise compatible dataset.

    Returns:
        ratings_ds: Surprise Dataset
    """
    reader = Reader(line_format="user item rating timestamp", sep=",", skip_lines=1)
    ratings_ds = Dataset.load_from_file("data/ratings.csv", reader=reader)

    return ratings_ds

def ratings_sim_mat(training_set):
    """
    Compute a similarity matrix.

    Args:
        training_set: Surprise TS to use

    Returns:
        similarity_matrix: Similarity Matrix
    """
    # Build similarity matrix
    similarity_matrix = KNNBasic(sim_options={
        'name': 'cosine',
        'user_based': False
    })\
    .fit(training_set)\
    .compute_similarities()

    return similarity_matrix

def cos_sim_setup():
    """
    Load ratings.csv dataset into full trainingset,
    then fit item-based cosine-similarity-matrix.

    Returns:
        sim_mat: Similarity Matrix
    """
    
    dataset = load_ds() 
    trainset = dataset.build_full_trainset()
    similarity_matrix = ratings_sim_mat(trainset)

    print(f"similarity_matrix type: {type(similarity_matrix[0])}")
    for el in similarity_matrix:
        #print(f"element type {type(el)}: {el.tolist()}")
        print(f"element type {type(el)}: {el.shape}")
    
    return (similarity_matrix, trainset, dataset)

def get_id_resolver():
    """
    Generate a movie rid->title lookup function with implicit dict handling.

    Returns:
        lookup_function (func): id->title resolver
    """

    id_name_dict = {}
    with open("data/movies.csv", newline="\n", encoding="UTF-8") as csvfile:
        movie_reader = csv.reader(csvfile)
        next(movie_reader)
        for row in movie_reader:
            movie_ID = int(row[0])
            movie_name = row[1]
            id_name_dict[movie_ID] = movie_name

    lookup_function = lambda _self, movie_rid: id_name_dict[int(movie_rid)]

    return lookup_function




###################
# USER CLASS
###################
class RecommenderUser(object):
    """
    User Class

    Class Attributes:
        similarity_matrix, trainset, _dataset: Surprise-library Algorithm objects
        movie_id_resolver (func): func to get a movieID's title
        k (int): default number of highest user ratings to use to recommend
    Instance Attributes:
        raw_uid (str): the outer id of the user
        inner_uid (int): uid inside the surprise training-set
        ratings (list): (itemID, rating) tuples of user's ratings
        k (int): adjusted class attribute k
        top_k_ratings (list): k highest rated items of user -> basis of recommendation

        candidates (dict): (itemID -> recommend-score) of all items
        sim_sorted_items (list): itemID:score tuples of unrated items, descending by score
        avg_score / median_score (int): baseline scores for normalizing
        recommendations (pandas.DataFrame): recommended items

    Methods:
        __init__(raw_uid, k): Constructor
        top_k_ratings_cos_sim_sum(k): compiles items into dict ranked by similarity score
        recommend(num_recs, detailed): Runs a recommendation for the user
    """


    # TODO make instance attrs if the wait is too long (or use ratings_compact.csv)
    # Class attributes -> only depend on prior ratings
    # -> computate on import, reducing waiting time.
    # Assumes no data-changes during runtime.
    similarity_matrix, trainset, _dataset = cos_sim_setup()
    movie_id_resolver = get_id_resolver()
    k = 20

    def __init__(self, **dict_configs):
        """
        Alterntive constructor passing raw_uid, k, ratings, top_k_ratings 
        as config_dict items directly
        """
        self.__dict__.update(dict_configs)
    
    def __init__(self, raw_uid: str, k: int = 20):
        """
        Instantiate uid-specific attributes

        Args:
            raw_uid (str): uid
            k (int): number of highest ratings used for recommender
        """
        self.raw_uid = raw_uid
        self.inner_uid = self.trainset.to_inner_uid(raw_uid)
        self.ratings = self.trainset.ur[self.inner_uid]
        self.k = min(20, len(self.ratings))
        self.top_k_ratings = heapq.nlargest(k, self.ratings, key=lambda t: t[1])


    def top_k_ratings_cos_sim_sum(self, k: int = 20): 
        """
        Rank all items based on summed cos-sim to user's top-k rated items

        Args:
            k (int): number of considered item-ratings
        Returns:
            candidates (dict): itemID -> computed relevance score
        """

        # Analogue numpy Version 
        n_items = len(self.similarity_matrix[0])
        itemsIDs = [*range(n_items)] # positional itemIDs
        items_cosims_sum = np.zeros((n_items), dtype=float)#[0.] * len(self.similarity_matrix[0]) # init sum of k similarity vectors
        for itemID, rating in self.top_k_ratings:
            items_cosims_sum = np.add(items_cosims_sum, np.asarray(self.similarity_matrix[itemID] * (rating / 5.0)))
            #items_cosims_sum.add(np.asarray(self.similarity_matrix[itemID] * (rating / 5.0) ))  #+= self.similarity_matrix[itemID] * (rating / 5.0)
        print(f"item id type: {type(itemsIDs)}")
        ziplist = zip(itemsIDs, items_cosims_sum.tolist())
        candidates = dict(ziplist)

        self.candidates = candidates
        return candidates

    def recommend(self, num_recs: int = 10, detailed: bool=False):
        """
        Recommend num_recs items with highest summed cos-sim to top k rated items

        Args:
            num_recs (int): n recommendations to give -> set to -1 to ix all items ranked
            detailed (bool): optional higher detail output
        Returns:
            df (pd.DataFrame): recommendations
        """
        candidates = self.top_k_ratings_cos_sim_sum()

        # Filter out user's rated items
        unseen_candidates = dict(list(candidates.items()))
        for inner_iid in self.ratings[0]:
            raw_iid = int(self.trainset.to_raw_iid(inner_iid))
            del unseen_candidates[raw_iid]

        # Sort ranked items from dict into desc. score list
        self.sim_sorted_items = sorted(unseen_candidates.items(), key=itemgetter(1), reverse=True)
        # compute avg / mean to normalize scores with
        n_items = int(len(self.sim_sorted_items))
        self.avg_score = sum(unseen_candidates.keys()) / n_items
        self.median_score = self.sim_sorted_items[int(n_items/2)]


        # Compile highest scoring user-recommendations
        recommendations = []
        for itemID, rating_sum in self.sim_sorted_items[:num_recs]:
            movie_rid = self.trainset.to_raw_iid(itemID)
            movie_title = self.movie_id_resolver(movie_rid)
            relevance_score = rating_sum
            recommendations.append((movie_title, movie_rid, relevance_score))
       
        # Create recommendations df
        df = pd.DataFrame(recommendations, columns=['title', 'movieID', 'score'])
        
        # Get time of recommendation
        current_time = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
        print(f"current time: {current_time}")
       
        # optional, additional output 
        detailed_flag_char = ""
        if detailed:
            df['score_by_avg']=df['score']/self.avg_score
            df['score_by_median']= df['score']/self.median_score
            #df['timestamp'] = current_time
            df['userID']=self.raw_uid
            detailed_flag_char="detailed"
        
        # write df to csv
        if not os.path.exists('user_data'): os.makedirs('user_data')
        filename = f"user_data/{detailed_flag_char}_user_{self.raw_uid}_recommendations_{current_time}.csv"
        if not os.path.exists(filename):
            df.to_csv(filename, sep=',', index=True, encoding='utf-8')

        self.recommendations = df
        return 



        
if __name__ == '__main__':

    test_user = RecommenderUser(raw_uid="500")
    test_user.recommend()
 