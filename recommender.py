# Contains parts from: https://flask-user.readthedocs.io/en/latest/quickstart_app.html
from flask import Flask, render_template, request, url_for
from flask_user import login_required, UserManager, current_user

from models import db, User, Movie, Rating
from read_data import check_and_read_data

import traceback

# Recommender Class and functions import
#from reco_class import *
import sim_class
from datetime import datetime
import time

# import sleep from python
from time import sleep

# Class-based application configuration
class ConfigClass(object):
    """ Flask application config """

    # Flask settings
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'

    # Flask-SQLAlchemy settings
    SQLALCHEMY_DATABASE_URI = 'sqlite:///movie_recommender.sqlite'  # File-based SQL database
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # Avoids SQLAlchemy warning

    # Flask-User settings
    USER_APP_NAME = "Movie Recommender"  # Shown in and email templates and page footers
    USER_ENABLE_EMAIL = False  # Disable email authentication
    USER_ENABLE_USERNAME = True  # Enable username authentication
    USER_REQUIRE_RETYPE_PASSWORD = True  # Simplify register form

    # make sure we redirect to home view, not /
    # (otherwise paths for registering, login and logout will not work on the server)
    USER_AFTER_LOGIN_ENDPOINT = 'home_page'
    USER_AFTER_LOGOUT_ENDPOINT = 'home_page'
    USER_AFTER_REGISTER_ENDPOINT = 'home_page'
    USER_AFTER_CONFIRM_ENDPOINT = 'home_page'

# Create Flask app
app = Flask(__name__)
app.config.from_object(__name__ + '.ConfigClass')  # configuration
app.app_context().push()  # create an app context before initializing db
db.init_app(app)  # initialize database
db.create_all()  # create database if necessary
user_manager = UserManager(app, db, User)  # initialize Flask-User management

@app.cli.command('initdb')
def initdb_command():
    global db
    """Creates the database tables."""
    check_and_read_data(db)
    print('Initialized the database.')

# favicon for browsers
@app.route("/favicon.ico")
def favicon():
    return url_for('static', filename="favicon.ico")

# The Home page is accessible to anyone
@app.route('/')
def home_page():
        
    # render home.html template
    return render_template("home.html")


# The Members page is only accessible to authenticated users via the @login_required decorator
@app.route('/movies')
@login_required  # User must be authenticated
def movies_page():
    # String-based templates
    # first 10 movies
    movies = Movie.query.limit(10).all()

    # only Romance movies
    # movies = Movie.query.filter(Movie.genres.any(MovieGenre.genre == 'Romance')).limit(10).all()

    # only Romance AND Horror movies
    # movies = Movie.query\
    #     .filter(Movie.genres.any(MovieGenre.genre == 'Romance')) \
    #     .filter(Movie.genres.any(MovieGenre.genre == 'Horror')) \
    #     .limit(10).all()
  
    return render_template("movies.html", movies=movies)


@app.route('/rate', methods=['POST'])
@login_required  # User must be authenticated
def rate():

    # Get Rating model Properties
    movie_id = request.form.get('movieid')
    score = request.form.get('rating')
    user_id = current_user.id
    movie_title = Movie.query.get(movie_id).title
    date_time_tuple = datetime.now().timetuple()
    timestamp = str(int(time.mktime(date_time_tuple) ))
    
    
    # Commit Rating to db
    rating = Rating(user_id=user_id, movie_id=movie_id, rating=score, timestamp=timestamp)
    db.session.add(rating)
    db.session.commit()
    #test back
    sess_rating = Rating.query.filter_by(user_id=user_id).filter(movie_id=movie_id).all()
   
    #TODO temporary before Rating model has __repr__() and db-reinit
    return render_template("rated.html", rating=sess_rating)


@app.route('/recommend', methods=['POST'])
@login_required  # User must be authenticated
def recommend():
    
    user = User.query.get(current_user.id)
    ratings = Rating.query.filter_by(user_id=user.id).order_by(Rating.rating.desc()).all()
    
    # "Un-sql-ify" ratings into skl-surprise's format
    ratings_list = []
    for r in ratings:
        ratings_list.append([int(r.movie_id), float(r.rating) ])
        
    starter = sim_class.Reco_starter(raw_uid = str(user.id), ratings=ratings_list, k=20 )
    recommendation_ids = starter.recommend(num_recs=10)['movieID']
    
    recommendations = Movie.query.filter(Movie.id.in_(recommendation_ids)).all()
    
    return render_template('recommendations.html', movies = recommendations)


@app.errorhandler(500)
def internal_error(exception):
   return "<pre>"+traceback.format_exc()+"</pre>"

# Start development web server
if __name__ == '__main__':
    app.run(port=5000, debug=True)
