from surprise import KNNBasic, Dataset, Reader
import csv
import os
import heapq
from collections import defaultdict
from operator import itemgetter
import pandas as pd
from datetime import datetime

# SURPRISE COLLABORATIVE FILTERING FUNCTIONS
##############

def load_ds():
    """
    Read ratings.csv into a surprise compatible dataset.

    Returns:
        ratings_ds: Surprise Dataset
    """
    reader = Reader(line_format="user item rating timestamp", sep=",", skip_lines=1)
    ratings_ds = Dataset.load_from_file("data/ratings.csv", reader=reader)

    return ratings_ds

def ratings_sim_mat(training_set):
    """
    Compute a similarity matrix.

    Args:
        training_set: Surprise TS to use

    Returns:
        similarity_matrix: Similarity Matrix
    """
    print("Entered ratings_sim_mat()")
    # Build similarity matrix
    similarity_matrix = KNNBasic(sim_options={
        'name': 'cosine',
        'user_based': False
    })\
    .fit(training_set)\
    .compute_similarities()

    return similarity_matrix

def cos_sim_setup():
    """
    Load ratings trainingset and fit cosine similarity matrix on it.

    Returns:
        sim_mat: Similarity Matrix
    """
    # read ratings.csv as dataset
    dataset = load_ds() # second return, if added later
    # build full Surprise trainingset 
    trainset = dataset.build_full_trainset()
    # compute similarity matrix
    similarity_matrix = ratings_sim_mat(trainset)

    print("b")
    #return similarity_matrix # 
    return (similarity_matrix, trainset, dataset)

# HELPER FUNCTIONS (IDs/NAMES)
##############
def get_id_resolver():
    """
    Generate a movie rid->title lookup function with implicit dict handling.

    Returns:
        lookup_function (func): id->title resolver
    """
    #id_name_dict = id_title_dict()
    id_name_dict = {}
    with open("data/movies.csv", newline="\n", encoding="UTF-8") as csvfile:
        movie_reader = csv.reader(csvfile)
        next(movie_reader)
        for row in movie_reader:
            movie_ID = int(row[0])
            movie_name = row[1]
            id_name_dict[movie_ID] = movie_name

    lookup_function = lambda movie_rid: id_name_dict[int(movie_rid)]
    #def lookup_function(movie_rid: int):

    return lookup_function

#def user_k_top_rated_items_item_sims(training_set, similarity_matrix, raw_uid: str, k: int = 20):
def top_k_rating_candidates(training_set, similarity_matrix, raw_uid: str, k: int = 20):

    """
    Get user-raw_uid's k top rated items (k_neighbors).
    Sort all items by similarity to k_neighbors in candidates.

    Args:
        training_set: Surprise TS that is used
        similarity_matrix: Surprise Sim_mat used
        raw_uid (str): raw id of user to recommend to
        k (int): considered number of users's highest ratings
    Returns:
        candidates (defaultdict): {str item_id: float relevance} of all items
        k_neighbors (list): 
    """

    # Get user's inner id (in training_set)
    inner_uid = training_set.to_inner_uid(str(raw_uid)) 
    # Get top K items we rated
    user_ratings = training_set.ur[inner_uid] 
    # -> gives inner-id's user's ratings as (item_inner_id: rating) Tuple generator
    k_neighbors = heapq.nlargest(k, user_ratings, key=lambda t: t[1])
    # -> fetches the user's ratings by key (largest criterion right?) t: t[1] <- rating position in trainset.ur[i_uid] tuples 

    # Defaultdict's add unknown keys instead of crashing
    candidates = defaultdict(float)
    # iterate over top-k-user-items and sort all items into candidates 
    # by user-rating-weighted sum of similarities
    for itemID, rating in k_neighbors:
        try:
            similarities = similarity_matrix[itemID] # vector of similarities to items
            for innerID, score in enumerate(similarities):
                candidates[innerID] += score * (rating / 5.0) 
        except:
            continue
    

    return candidates #, user_ratings, k_neighbors #sorted(list(candidates.items, reverse=True) 


def recommendation_process(raw_uid="500", k=20, num_recs=10, return_all=False, write_csv=True):
    """
    Recommend num_recs movies, by ranking items by:
    cosine-similarity to user's k highest-rated movies.
    Write recommendations to user_data/user_recs.csv and/or return as pandas df.

    Args:
        raw_uid (str): uid of user to recommend for
        k (int): number of considered highest ratings 
        num_recs (int): number of recommendations to return

    Returns:
        recommendations (pandas.Dataframe): highest scoring items
    """


    # Set num_recs to max index if all items should be returned
    if return_all:
        num_recs = -1

    # get movie raw_id->title resolver function
    movie_id_resolver = get_id_resolver()

    # get trainset and similarity matrix
    similarity_matrix, trainset, _dataset = cos_sim_setup()

    # Get user and ratings in trainset (redundant but central)
    inner_uid = trainset.to_inner_uid(str(raw_uid)) 
    user_ratings = trainset.ur[inner_uid]

    # get dict of all {itemID: relevance score}s, convert to score-desc. list
    candidates = top_k_rating_candidates(trainset, similarity_matrix, raw_uid, k)
    
    # Filter out user's rated items
    for rated in user_ratings:
        del candidates[rated[0]]
        #candidates.del(rated[0])

    sim_sorted_items = sorted(candidates.items(), key=itemgetter(1), reverse=True)
    # compute avg / mean relevance score to normalize recommendation scores later
    n_items = int(len(sim_sorted_items))
    avg_score = sum(candidates.keys()) / n_items
    median_score = sim_sorted_items[int(n_items/2)]

    # get inner uid
    inner_uid = trainset.to_inner_uid(raw_uid)

    # Build dict of movies user has watched
    watched = {}
    for itemID, rating in trainset.ur[inner_uid]:
        watched[itemID] = 1

    # filter out items the user rated
    sim_sorted_items = []


    # Add items to list of user-recommends
    # I they're similar to their fav movies AND haven't been watched before
    recommendations = []
    for itemID, rating_sum in sim_sorted_items[:num_recs]:
        if not itemID in watched:
            movie_rid = trainset.to_raw_iid(itemID)
            movie_title = movie_id_resolver(movie_rid)
            relevance_score = rating_sum
            recommendations.append((movie_title, movie_rid, relevance_score))

            score_by_avg = relevance_score / avg_score
            score_by_median = relevance_score / median_score
        
    for rec in recommendations:
        print(f"Movie: {rec[0]}, id: {rec[1]}, score:{rec[2]}")

    df = pd.DataFrame(recommendations, columns=['title', 'movieID', 'score'])
  
    # TODO include entire list of similarities? or at least more if users want to explore more.
    if detailed:
        df['score_by_avg']=df['score']/avg_score
        df['score_by_median']= df['score']/median_score
        detailed_df = pd.DataFrame(recommendations)

    current_time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    print(f"current time: {current_time}")
    #df.to_csv(f"user_data/{raw_uid}_recommendations.csv", sep=',', index=True, encoding='utf-8')

if __name__ == '__main__':
    recommendation_process()
    #id_resolver = get_id_resolver()
    #print(f"Movie with id {3862} resovles to {id_resolver(3862)}")
